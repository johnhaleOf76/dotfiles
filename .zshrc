# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# added from zsh error popup
typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet

# This has to do with autoupdating the CUSTOM plugins.
# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=9
#ZSH_CUSTOM_AUTOUPDATE_QUIET=true


# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
# This might fix npm running an update script inside the modules instead of my update script
export NODE_PATH=$NODE_PATH:$HOME/.npm-global/lib/node_modules

# Path to your oh-my-zsh installation.
export ZSH="/home/john/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
 HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
 ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
 HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    autoupdate
    alias-finder
    sudo
    zsh_reload
    zsh-syntax-highlighting
    common-aliases
    git
    zsh-autosuggestions
    npm
    nvm
    node 
    systemd
    battery 
    colored-man-pages
    ubuntu
    nmap
    web-search 
    fzf
    fzf-tab
    z
    extract
    vscode
    deno
    rust
    
    
)
# web-search uses !w !n !yt !i !m ! = ducky or start with ddg or other

# toggle-fzf-tab  // ctrl+space to select multi  //  / to continuous completions

# If issues open oh-my-zsh/plugins ... vim/nerdtree

# This should change the fzf window size and shape
# let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }

# Edit line in vim with ctrl-v:
autoload edit-command-line; zle -N edit-command-line
bindkey '^v' edit-command-line


source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
 export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
 alias zshconfig="vim ~/.zshrc"
 alias ohmyzsh="vim ~/.oh-my-zsh"

# This is for my gitlab back-up files
alias config='/usr/bin/git --git-dir=$HOME/Documents/dotfiles/ --work-tree=$HOME'

# switch between shells / This is from DT .. Deric Taylor
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"


# These are a few I made for FZF
# This is a search apt and install script
alias fzfI="apt-cache search '' | sort | cut --delimiter ' ' --fields 1 | fzf --multi --cycle --reverse --preview 'apt-cache show {1}' | xargs -r sudo apt install -y"
# You can preview what you are searching for.
alias fzfP="fzf --preview 'bat --style=numbers --color=always --line-range :1000 {}'"
alias fzfOpen='editor $(find * -type f | fzf)'
# TESTING A NEW SCRIPT // combination of 2 other fzf aliase

# alias fzfz="fzf --preview 'bat --style=numbers --color=always --line-range :1000 {}' | editor $(find -type f)"

# alias ffzz="editor $(find * -type f | fzf --preview 'bat --style=numbers --color=always --line-range :1000 {}')"

# This should be integrated with my update script in the home directory
# ./update: update all of my packages AND omz, my zsh custom packages, and whatever else I can think of.
if [ ! -z "$(which apt)" ]; then
    alias update="sudo apt update && sudo apt upgrade && sudo apt full-upgrade"
elif [ ! -z "$(which apt-get)" ]; then
    alias update="sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade"
fi

# myip
alias myip="curl icanhazip.com"

alias lsm="ls -hlAFG"

# Weather for the CLI
weather() { curl wttr.in/"$1"; }

ZSH_ALIAS_FINDER_AUTOMATIC=true
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

#curl https://cheat.sh/:zsh > ~/.zsh.d/_cht
# echo 'fpath=(~/.zsh.d/ $fpath)' >> ~/.zshrcfpath=(~/.zsh.d/ $fpath)

# go
export PATH=$PATH:/usr/local/go/bin


# deno
  export DENO_INSTALL="/home/john/.deno"
  export PATH="$DENO_INSTALL/bin:$PATH"

   export PATH=~/.deno/bin/deno:$PATH


