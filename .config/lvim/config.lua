--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT

-- general
lvim.log.level = "warn"
lvim.format_on_save = true
-- lvim.colorscheme = "onedarker"
-- Catppuccino colorscemes "dark_catppuccino", "neon_latte", "soft_manilo", "light_melya"
-- lvim.colorscheme = "dark_catppuccino"
-- lvim.colorscheme = "neon_latte"
-- lvim.colorscheme = "soft_manilo"
-- lvim.colorscheme = "light_melya"
-- lvim.colorscheme = "tokyonight"
lvim.colorscheme = ' github_*'
-- keymappings [view all the defaults by pressing <leader>Lk]

lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
-- unmap a default keymapping
-- lvim.keys.normal_mode["<C-Up>"] = ""
-- edit a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
 lvim.builtin.telescope.on_config_done = function()
   local actions = require "telescope.actions"
--   -- for input mode
   lvim.builtin.telescope.defaults.mappings.i["<C-j>"] = actions.move_selection_next
   lvim.builtin.telescope.defaults.mappings.i["<C-k>"] = actions.move_selection_previous
   lvim.builtin.telescope.defaults.mappings.i["<C-n>"] = actions.cycle_history_next
   lvim.builtin.telescope.defaults.mappings.i["<C-p>"] = actions.cycle_history_prev
--   -- for normal mode
   lvim.builtin.telescope.defaults.mappings.n["<C-j>"] = actions.move_selection_next
   lvim.builtin.telescope.defaults.mappings.n["<C-k>"] = actions.move_selection_previous
 end

-- Use which-key to add extra bindings with the leader-key prefix
 lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
 lvim.builtin.which_key.mappings["t"] = {
   name = "+Trouble",
   r = { "<cmd>Trouble lsp_references<cr>", "References" },
   f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
   d = { "<cmd>Trouble lsp_document_diagnostics<cr>", "Diagnostics" },
   q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
   l = { "<cmd>Trouble loclist<cr>", "LocationList" },
   w = { "<cmd>Trouble lsp_workspace_diagnostics<cr>", "Diagnostics" },
 }

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.dashboard.active = true
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.side = "left"
lvim.builtin.nvimtree.show_icons.git = 0

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {}
lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true
lvim.builtin.treesitter.matchup.enable = true
lvim.builtin.treesitter.indent = { enable=true, diable={'yaml', "python"}}


-- generic LSP settings
-- you can set a custom on_attach function that will be used for all the language servers
-- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end
-- you can overwrite the null_ls setup table (useful for setting the root_dir function)
-- lvim.lsp.null_ls.setup = {
--   root_dir = require("lspconfig").util.root_pattern("Makefile", ".git", "node_modules"),
-- }
-- or if you need something more advanced
-- lvim.lsp.null_ls.setup.root_dir = function(fname)
--   if vim.bo.filetype == "javascript" then
--     return require("lspconfig/util").root_pattern("Makefile", ".git", "node_modules")(fname)
--       or require("lspconfig/util").path.dirname(fname)
--   elseif vim.bo.filetype == "php" then
--     return require("lspconfig/util").root_pattern("Makefile", ".git", "composer.json")(fname) or vim.fn.getcwd()
--   else
--     return require("lspconfig/util").root_pattern("Makefile", ".git")(fname) or require("lspconfig/util").path.dirname(fname)
--   end
-- end

-- set a formatter if you want to override the default lsp one (if it exists)
-- lvim.lang.python.formatters = {
--   {
--     exe = "black",
--   }
-- }
-- set an additional linter
-- lvim.lang.python.linters = {
--   {
--     exe = "flake8",
--   }
-- }

-- Sh Linter
lvim.lang.sh.linters = { { exe = 'shellcheck' } }

-- Lua linters and Formatters
 lvim.lang.lua.linters = { { exe = 'selene'} }
-- lvim.lang.lua.formatters = { { exe = 'styla' } }

-- JavaScript Formatters
-- exe value can be "prettier", "prettierd", "eslint", or "eslint_d"
lvim.lang.javascript.formatters = { { exe = "prettier"}, { exe = "eslint"} }
lvim.lang.javascriptreact.formatters = lvim.lang.javascript.formatters
-- JavaScript Linters
-- exe value can be "eslint", or "eslint_d"
lvim.lang.javascript.linters = { { exe = "eslint"} }
lvim.lang.javascriptreact.linters = lvim.lang.javascript.linters



-- Additional Plugins
 lvim.plugins = {
     {"folke/tokyonight.nvim"},
     { "Pocco81/Catppuccino.nvim" },
     { "romgrk/doom-one.vim" },
     { "projekt0n/github-nvim-theme" },
--     { "ellisonlwao/glow.nvim" },
     { "npxbr/glow.nvim",
       ft = {"markdown"}},
     { "turbio/bracey.vim",
        cmd = {"Bracey", "BraceyStop", "BraceyReload", "BraceyEval"},
        run = "npm install --prefix server",},

     {
       "folke/trouble.nvim",
       cmd = "TroubleToggle",
     },
     { "lunarvim/colorschemes" },
     { "shaunsingh/nord.nvim" },
--     { "mfussenegger/nvim-jdtls" },
        { "sainnhe/gruvbox-material" },
        { "p00f/nvim-ts-rainbow" },
        -- { "sudormrfbin/cheatsheet.nvim",
        -- requires = {
        --         {'nvim-telescope/telescope.nvim'},
        --         {'nvim-lua/popup.nvim'},
        --         {'nvim-lua/plenary.nvim'},},
        { "iamcco/markdown-preview.nvim",
        run = "cd app && npm install",
        ft = "markdown",
        config = function()
         vim.g.mkdp_auto_start = 1
                end,},
        { 'sudormrfbin/cheatsheet.nvim',
  requires = {
    {'nvim-telescope/telescope.nvim'},
    {'nvim-lua/popup.nvim'},
    {'nvim-lua/plenary.nvim'},
  }
},
     { "vimwiki/vimwiki" },
--     { 'chipsenbeil/vimwiki-server.nvim', { 'tag': 'v0.1.0-alpha.5' } },

 };





lvim.vimwiki_autowriteall=1
--opt hidden = true

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
lvim.autocommands.custom_groups = {
   { "BufWinEnter", "*.lua", "setlocal ts=8 sw=8" },
 };
