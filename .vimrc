" Hacked together by John Hale in 2020 This is my 1st vimrc 
" Keep working on it.

set nocompatible
syntax on
scriptencoding utf=8
set spelllang=en_us            " Set the spellchecking language
filetype plugin indent on
set hidden
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu                        "show numbers
set relativenumber            " Show line numbers relative to cursor position, 
                               " this is useful to move between lines.   								   
                                " Disable temp with :set nornu
set hlsearch                  "highlight search
set ignorecase
set smartcase                 " Override `ignorecase` option
                              " if the search pattern contains
                              " uppercase characters
                              
set incsearch                 " Highlight search pattern as
                              " it is being typed
" Undo directory
set undodir=~/.vim/undodir
set undofile
set matchpairs+=<:>             " Match < and > as well.
set autoindent
set mouse=a
set splitright
set splitbelow
set backspace=indent,eol,start  "allow backspace in insert mode
set ruler                     "show the cursor all the time 
set rtp+=~/.fzf
set autoread                  "reload files automagically
set showmode                  "show current mode
set showcmd                   "show incomplete commands
set cmdheight=2               " More space for messages
set updatetime=350            "Longer time to update

set wildmenu                  " Enable enhanced command-line
                              " completion (by hitting <TAB> in
                              " command mode, Vim will show the 
                              " possible matches just above the 
                              "command line with the first match highlighted)
set wildmode=list:longest,full

set winminheight=0             " Allow windows to be squashed

imap jj <Esc>

"let g:user_emmet_mode='n'    "enable normal functions. 
"let g:user_emmet_mode='inv'  "enable all functions, which is equal to
let g:user_emmet_mode='a'    "enable all function in all mode 

let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1

" This has to do with color"
set laststatus=2               " Always show the status line

set colorcolumn=80
set termguicolors
set background=dark
highlight ColorColumn ctermbg=0 guibg=lightgrey
" colorscheme: space-vim-dark
hi Comment cterm=italic
"colorscheme gruvbox

"let g:eleline_slim=0 "slim line ctrl G"

let g:github_dashboard = { 'username': ' ', 'password': ' ' }

" Organize this down to what you need / speed it up
call plug#begin('~/.vim/plugged')

Plug 'vim-utils/vim-man'
Plug 'vimwiki/vimwiki'

Plug 'https://github.com/Valloric/YouCompleteMe.git'
Plug 'https://github.com/junegunn/vim-github-dashboard.git'
" FZF
Plug 'https://github.com/junegunn/fzf',{ 'do':{ -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf'

Plug 'vim/killersheep'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'severin-lemaignan/vim-minimap'

" Nerdtree
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'preservim/nerdtree'

Plug 'francoiscabrol/ranger.vim'
" this is a dependency to open ranger instead of nerdtree
Plug 'rbgrouleff/bclose.vim'

" Themes and Colors
Plug 'liuchengxu/eleline.vim'
Plug 'liuchengxu/space-vim-dark'
Plug 'bluz71/vim-moonfly-statusline'
Plug 'bluz71/vim-nightfly-guicolors'
Plug 'morhetz/gruvbox'
Plug 'gko/vim-coloresque'
Plug 'flazz/vim-colorschemes'
Plug 'ryanoasis/vim-devicons'
Plug 'liuchengxu/nerdtree-dash'
Plug 'navarasu/onedark.nvim'

" Programming / Lang
Plug 'instant-markdown/vim-instant-markdown', {'for': 'markdown', 'do': 'yarn install'}
Plug 'yuezk/vim-js'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'}
Plug 'https://github.com/mattn/emmet-vim'
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown'
Plug 'honza/vim-snippets'
" Pandoc
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'

" tabnine autocomplete for languages
Plug 'zxqfl/tabnine-vim'
Plug 'codota/tabnine-vim'

call plug#end()
" Lvim test
let g:nvim_tree_auto_open = '1'
let g:nvim_tree_hide_dotfiles = '0'
let g:nvim_tree_highlight_opened_files = '3'



" Ranger file manager
" let g:ranger_replace_netrw = 1 // open ranger when vim open a directory
let g:NERDTreeHijackNetrw = 0 "// add this line if you use NERDTree

" Hotkey to toggle filetree
" #map<F5>:NERDTreeToggle<CR>
" F3 works for NerdTree ?? Not F5 ???

" This should show nerdTree at start ???
let NERDTreeShowHidden=1

" Start NERDTree and put the cursor back in the other window.
" autocmd VimEnter * NERDTree | wincmd p

" PDF viewer with Pandoc
let g:md_pdf_viewer="envice"
let g:md_args = "--template eisvogel --listings"

" =========== VIMWIKI ===========
" I'm not sure if this is going to work the way that I want it to in SpaceVim
" GOT RID OF SPACEVIM

let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

let g:vimwiki_global_ext = 0

" This should autocomplete JS  on the fly
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS

" ------ for markdown preview --------Markdown
let g:mkdp_refresh_slow = 1
let g:mkdp_command_for_global = 1

" instant markdown preview
filetype plugin on
"Uncomment to override defaults:
let g:instant_markdown_slow = 1
let g:instant_markdown_autostart = 0
"let g:instant_markdown_open_to_the_world = 1
let g:instant_markdown_allow_unsafe_content = 1
let g:instant_markdown_allow_external_content = 0
"let g:instant_markdown_mathjax = 1
"let g:instant_markdown_mermaid = 1
let g:instant_markdown_logfile = '/tmp/instant_markdown.log'
"let g:instant_markdown_autoscroll = 0
"let g:instant_markdown_port = 8888
"let g:instant_markdown_python = 1


let g:moonflyIgoreDefaultColors = 1
let g:gruvbox_contrast_dark = 'hard'
let g:minimap_highlight='Visual'
" ==============================================================
" FZF
" =============================================================
" This is the default option:
"   - Preview window on the right with 50% width
"   - CTRL-/ will toggle preview window.
" - Note that this array is passed as arguments to fzf#vim#with_preview function.
" - To learn more about preview window options, see `--preview-window` section of `man fzf`.
let g:fzf_preview_window = ['right:50%', 'ctrl-/']

" fzf popup window /// not sure where to put this // 
 let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.8 } }

:command! OpenInVSCode exe "silent !code --goto '" . expand("%") . ":" . line(".") . ":" . col(".") . "'" | redraw!

let g:onedark_style = 'darker'  "warm, warmer, cool, deep, darker -- We need add the configs before colorscheme line
" colorscheme onedark
colorscheme = gruvbox
let mapleader = "\<Space>"

