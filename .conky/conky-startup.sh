#!/bin/sh

if [ "$DESKTOP_SESSION" = "budgie-desktop" ]; then 
   sleep 30s
   killall conky
   cd "$HOME/.config/conky"
   conky -c "$HOME/.config/conky/conky.conf" &
   cd "$HOME/.conky/Extra/Gotham"
   conky -c "$HOME/.conky/Extra/Gotham/Gotham" &
   exit 0
fi
if [ "$DESKTOP_SESSION" = "awesome" ]; then 
   # No widgets enabled!
   exit 0
fi
